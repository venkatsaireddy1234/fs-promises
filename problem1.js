const fs = require("fs");
const path = require("path");
const { resolve } = require("path");

function makedir(path) {
    return new Promise((resolve,reject)=>{
        fs.mkdir(path, (err) => {
            if (err) {
                reject(err);
            } else {
            resolve("Jsonfolder created");
            }
        });
    })
}


function creatingfiles(filePath) {
    return new Promise((resolve,reject)=>{
        for (let i = 0; i < filePath.length; i++) {
            fs.writeFile(filePath[i], "Hi", (err) => {
                if (err) {
                reject(err);
                } else {
                if (i ==filePath.length-1){
                    resolve("Files created sucessfully")
                }
            }
        });
        }
    })    
}
  
        
function deletefile(path){
    return new Promise((resolve,reject)=>{
        for (let i=0;i<path.length;i++){
            fs.unlink(path[i],err=>{
                if(err){
                    reject(err)
                }else{
                    if (i ===path.length-1){
                    resolve(`JsonFiles deleted`)
                    }
                }
            })
        }
    })  
}          

                    
function operation(folder,filePath){
    makedir(folder)
    .then((data) =>{
        console.log(data);
        return creatingfiles(filePath)
    })
    .then((data) => {
        console.log(data);
        return deletefile(filePath);
      })
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      });
  
}

  
module.exports.operation=operation;







