const fs = require("fs")
const path = require("path")
//const { resolve } =require("path")

const lipsum_txt = path.resolve(__dirname,"./lipsum.txt")
const upperCase_txt = path.resolve(__dirname, "./upperCase.txt")
const filenames_txt = path.resolve(__dirname, "./filesnames.txt")
const sentence_txt = path.resolve(__dirname, "./sentence.txt")
const sortData_txt = path.resolve(__dirname, "./sortData.txt");



function readAndUpper(){
    return new Promise((resolve,reject)=>{
        fs.readFile(lipsum_txt,"utf8",(err,data)=>{
            data = data.toUpperCase()
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}



function addingUpperCase(data){
    return new Promise((resolve,reject)=>{
        fs.writeFile(upperCase_txt,data,(err)=>{
            if(err){
                reject(err)
            }else{
                resolve(upperCase_txt)
            }
        })
    })

}

function appendFile(files){
    return new Promise((resolve,reject)=>{
        fs.appendFile(filenames_txt, files +" ",(err)=>{
            if(err){
                reject(err)
            }else{
                resolve("Files appened successfully")
            }
        })
    })
}

function readUpperCaseandsplitit(upperCase_txt){
    return new Promise((resolve,reject)=>{
        fs.readFile(upperCase_txt,"utf8",(err,data)=>{
            data = data.toLowerCase().split(" ")
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}



function writeSentence(data){
    return new Promise((resolve,reject)=>{
        data.forEach(element => {
            fs.appendFile(sentence_txt, element+"\n" ,(err)=>{
                if(err){
                    reject(err)
                }else{
                    resolve(sentence_txt)
                }
            })
            
        });
    })
}



function readSentencedData(sentencedfile) {
    return new Promise((resolve, reject) => {
        fs.readFile(sentencedfile, "utf-8", (err, data) => {
            let array = data.split('\n');
             let sorted = array.sort();
             sorted = sorted.join('\n').trim();
             if(err){
                 reject(err)
             }else{
                 resolve(sorted)
             }
        })
    })
}

function writeSortedData(sorted) {
    return new Promise((resolve, reject) => {
        fs.writeFile(sortData_txt, sorted, 'utf8',(err)=> {
            if(err){
                reject(err)
            }else{
                resolve(sortData_txt)
            }
        })
    })
}

function readAllFiles(filenames_txt){
    return new Promise((resolve, reject) => {
        fs.readFile(filenames_txt, "utf-8", (err, data) => {
            let arrayfiles = data.split(' ');
            if(err){
                reject(err)
            }else{
                resolve(arrayfiles)
            }
        })
    })
}

function deleteAllFiles(arrayfiles) {
    return new Promise((resolve, reject) => {
        arrayfiles.forEach((eachfile) => {
            if (eachfile != '') {
                fs.unlink(eachfile, (err) => {
                    if(err){
                        reject(err)
                    }else{
                        resolve("All files are deleted")
                    }
                })
            }
        })
    })
}

module.exports={readAndUpper,addingUpperCase,appendFile,readUpperCaseandsplitit,writeSentence,readSentencedData,writeSortedData,readAllFiles,deleteAllFiles,filenames_txt}